/*
 * Start the angular Application
 */

var nodeStripe = angular.module('nodeStripe', []);
/*
 * ANgular js Controller
 */
 
nodeStripe.controller('stripeController', stripeController);
function stripeController ($scope, $http) {
	/*
	 * Get the Current User
	 */
	$scope.getUser = function () {
		//alert('get');
	    $http.get('/auth').success(function (user) {
	        console.log('Current user is ', user);
	        $scope.user = user;
	    }).error(function (error) {
	        console.log('Error getting the current user ', error);
	    });
	}
}