Nodejs Application
========================

NodeJs Application with stripe checkout using monoglab database with json web token authentication.

a [Node](https://nodejs.org/en/) application
 
Installation
------------
```bash
Open terminal clone the repo 'git clone git@bitbucket.org:betasoftsystems/node-stripe.git'

$ cd node-stripe

$ npm -g install sails

$ npm install

$ sails lift
```
Installation
------------
```bash
Goto [App Home](http://localhost:1337/register).

Working Demo

https://node-stripe.herokuapp.com/

```