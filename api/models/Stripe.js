/**
* Stripe.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
  	checkoutId: {
      type: 'string'
    },
    amount: {
      type: 'integer'
    },
    description: {
    	type: 'string'
    },
    // Add a reference to User
    customer:{
        model:'user'
    }
  }
};

