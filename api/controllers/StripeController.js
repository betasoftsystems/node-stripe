/**
 * StripeController
 *
 * @description :: Server-side logic for managing stripes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	
	/*
	 * Save Values After Stripe Checkout
	 */
	afterCheckout: function(req, res) {
		var data = req.body;
		//console.log('data ', data);
		/* 
		 * Validate the API request
		 */
		//if(data.accessToken == req.user.accessToken) {

			var stripe = require("stripe")("sk_live_RzxfvrcdeIFTkC8maCyU1pkn"); //sk_live_RzxfvrcdeIFTkC8maCyU1pkn //sk_test_diJJIho6CLKQlCEdwfq1Ddfk
			var stripeToken = data.stripeToken;

			var charge = stripe.charges.create({
			  amount: data.amount, // amount in cents, again
			  currency: "usd",
			  source: stripeToken,
			  description: data.description
			}, function(err, charge) { 
			  if (err && err.type === 'StripeCardError') {
			    // The card has been declined
			    req.flash('error', err);
				res.redirect('/');
			  }else if(charge) {
			  		Stripe.create({
						checkoutId: charge.id,
						amount: data.amount,
						description: data.description,
						customer: req.user
					}).exec(function createCB(err, created) {
						req.flash('message', 'Thank you for the payment, Your transaction id is '+charge.id );
						res.redirect('/');
					});
			  }
			});
		// }else {
		// 	return res.forbidden('You are not permitted to perform this action.');
		// }
	}
};

