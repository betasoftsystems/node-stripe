var User = {
	// Enforce model schema in the case of schemaless databases
	schema: true,

	attributes: {
		username: {
			type: 'string',
			unique: true
		},
		email: {
			type: 'email',
			unique: true
		},
		passports: {
			collection: 'Passport',
			via: 'user'
		},
		user:{
            collection: 'stripe',
            via: 'customer'
        },
        accessToken: {
			type: 'string'
		},
	}
};

module.exports = User;